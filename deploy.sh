#!/bin/bash
#Author: Domenico Caravaggio
# automatically backup ans deploy
#
########################
#   CONFIG FILE LOG
DIR_LOG=''
LOG_PREFIX=''
FILE_LOG=${DIR_LOG}/${LOG_PREFIX}.log
#######################
#
EMAIL_SEND=""
#######################
#   CONFIG BACKUP
BASE_DIR=''
DIR_BACKUP=''
FILE_PREFIX=''
FILE_BACKUP=${DIR_BACKUP}/${FILE_PREFIX}-$( date +'%Y-%m-%d-%s' )
FILE_BACKUP_GZ=${FILE_BACKUP}.tar.gz
#####################
#   CONFIG DEPLOY
DIR_BASE=''
DIR_DEPLOY=''
DIR_BASE_NAME=''
FILE_FLAG=''
#######################
function loging {
        DATE=$( date )
        TEXT="${@}"
        echo "${DATE} - ${TEXT}" 1>&2
        echo "${DATE} - ${TEXT}" >> ${FILE_LOG}
}
#######################
# Main

loging " ------- INIT SCRIPT -------"
if [ -f "${DIR_DEPLOY}${FILE_FLAG}" ]; then
    loging " INFO: EXIST FILE  ${FILE_FLAG}"
    loging " INFO: RM  ${FILE_FLAG}"
    rm -r -f ${DIR_DEPLOY}${FILE_FLAG}
    loging " ------- config script -------"
    loging " FILE_BACKUP  ${FILE_BACKUP}"
    loging " FILE_LOG ${FILE_LOG}"
    loging " DIR_BASE ${DIR_BASE}"
    loging " -----------------------------"
    loging " ----------- EMAIL ------------"
    /usr/bin/echo "DEPLOY BASE" | /usr/bin/mail  -s "DEPLOY BASE" ${EMAIL_SEND}
    loging " ----------- CREATE DIR BACKUP ------------"
    mkdir -p ${FILE_BACKUP}
    loging " ----------- RSYINC FILE BACKUP ------------"
    if [ -d "${DIR_BASE}" ]; then
        loging " ${DIR_BASE} to ${FILE_BACKUP}/ "
        rsync -a -r --exclude='__pycache__' --exclude='.git' ${DIR_BASE}/${DIR_BASE_NAME} ${FILE_BACKUP}/
    fi
    loging " ----------- TAR GZ FILE  ------------"
    if [ ! -f "$FILE_BACKUP_GZ" ]; then
        loging " INFO: EXEC tar -zcvpf ${FILE_BACKUP_GZ} ${FILE_BACKUP}"
        tar -zcvpf ${FILE_BACKUP_GZ} ${FILE_BACKUP}
        loging " INFO: EXEC  rm -r -f ${FILE_BACKUP}"
        rm -r -f ${FILE_BACKUP}
    else
        loging "ERROR exists ${FILE_BACKUP_GZ}"
        loging " INFO: EXEC  rm -r -f ${FILE_BACKUP}"
        rm -r -f ${FILE_BACKUP}
        exit 1
    fi
    loging " ------- INIT DEPLOY -------"
    if [ -d "${DIR_DEPLOY}${DIR_BASE_NAME}" ]; then
        loging " INFO: EXIST DIRECTORY DEPLOY  ${DIR_DEPLOY}"
        rsync -a -r --exclude='__pycache__' --exclude='.git' ${DIR_DEPLOY}${DIR_BASE_NAME} ${DIR_BASE}
        chown -R apache.apache ${DIR_BASE}
        httpd -k graceful 1>&2
    else
        loging "ERROR exists ${DIR_DEPLOY}${DIR_BASE_NAME}"
    fi
loging " ------- END DEPLOY -------"
loging " ------ delete old BACKUP FILE and TMP FILE ------"
loging " find ${DIR_BACKUP} -type f -name "${FILE_PREFIX}" -mtime +7 -exec rm -f {} \; "
find ${DIR_BACKUP} -type f -name "${FILE_PREFIX}*.gz" -mtime +7 -exec rm -f {} \;
loging " rm ${DIR_DEPLOY}${DIR_BASE_NAME} "
rm -r -f ${DIR_DEPLOY}${DIR_BASE_NAME}
loging " ----------- EMAIL ------------"
/usr/bin/echo "END DEPLOY BASE" | /usr/bin/mail  -s "END DEPLOY BASE" ${EMAIL_SEND}
else
  loging " INFO: FILE  ${FILE_FLAG} NOT FOUND"
fi
loging " ------ EXIT SCRIPT ------"
exit 0
######################

